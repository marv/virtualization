Title: Docker 1.13.0 upgrade procedure
Author: Arnaud Lefebvre <arnaud.lefebvre@clever-cloud.com>
Content-Type: text/plain
Posted: 2017-01-26
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: app-virtualization/docker

Docker changed its plugin system in the 1.13.0 release.
If you never manually installed a plugin, then you're fine.

You have to remove all plugins before the update, otherwise docker won't start:

  docker plugin ls # list plugins
  docker plugin rm <plugin> # remove plugin

More informations here: https://github.com/docker/docker/releases/tag/v1.13.0
